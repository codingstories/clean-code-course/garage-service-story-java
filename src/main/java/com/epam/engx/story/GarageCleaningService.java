package com.epam.engx.story;

public interface GarageCleaningService {

    void clean(int garage);
}
