package com.epam.engx.story.model;

import java.util.Objects;

public class Car {

    private static final String CLASSIC_CAR_ABBREVIATION = "C";
    private static final String SIMPLE_CAR_ABBREVIATION = "S";

    private String manufacturer;
    private String model;
    private int year;
    private boolean classic;

    public Car(String manufacturer, String model, int year, boolean classic) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.classic = classic;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public boolean isClassic() {
        return classic;
    }

    @Override
    public String toString() {
        return String.format("%s %s [%s] %s", manufacturer, model, year,
                classic ? CLASSIC_CAR_ABBREVIATION : SIMPLE_CAR_ABBREVIATION);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return classic == car.classic &&
                Objects.equals(manufacturer, car.manufacturer) &&
                Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturer, model, classic);
    }
}
